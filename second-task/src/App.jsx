import React from "react";

import "./App.css";
import UserForm from "./UserForm/UserForm.tsx";

function App() {
  return (
    <div className="App">
      <UserForm></UserForm>
    </div>
  );
}

export default App;
