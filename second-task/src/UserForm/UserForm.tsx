import React, { useState } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import "./index.css";

const UserForm = () => {
  const initialValues = {
    firstName: "",
    lastName: "",
    age: "",
    employed: false,
    favoriteColor: "",
    sauces: [],
    bestStorage: "larry",
    notes: "",
  };

  const [submittedValues, setSubmittedValues] = useState("{}");
  const validationSchema = Yup.object().shape({
    firstName: Yup.string()
      .matches(
        /^[A-Za-z\s]+$/,
        "Only alphabet characters and spaces are allowed"
      )
      .required("Required"),
    lastName: Yup.string()
      .matches(
        /^[A-Za-z\s]+$/,
        "Only alphabet characters and spaces are allowed"
      )
      .required("Required"),
    age: Yup.number().typeError("Age must be a number").required("Required"),
    notes: Yup.string().max(100, "Notes cannot exceed 100 characters"),
  });

  const handleSubmit = (values, { resetForm }) => {
    setSubmittedValues(JSON.stringify(values, null, 2));

    resetForm();
  };

  return (
    <div className="user-form">
      <h1>User Form</h1>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {({ isSubmitting, isValid, dirty, resetForm }) => (
          <Form>
            <div className="form-field">
              <label htmlFor="firstName">First Name</label>
              <Field type="text" id="firstName" name="firstName" />
              <ErrorMessage
                name="firstName"
                component="div"
                className="error"
              />
            </div>
            <div className="form-field">
              <label htmlFor="lastName">Last Name</label>
              <Field type="text" id="lastName" name="lastName" />
              <ErrorMessage name="lastName" component="div" className="error" />
            </div>
            <div className="form-field">
              <label htmlFor="age">Age</label>
              <Field type="text" id="age" name="age" />
              <ErrorMessage name="age" component="div" className="error" />
            </div>
            <div className="form-field">
              <label htmlFor="age">Employed</label>
              <Field type="checkbox" name="employed" />
            </div>
            <div className="form-field">
              <label htmlFor="favoriteColor">Favorite Color</label>
              <Field as="select" id="favoriteColor" name="favoriteColor">
                <option value="" label="Select a color" />
                <option value="green" label="Green" />
                <option value="red" label="Red" />
                <option value="blue" label="Blue" />
              </Field>
            </div>
            <div className="form-field">
              <label>Sauces</label>
              <div className="sauces-checkbox">
                <label>
                  <Field type="checkbox" name="sauces" value="ketchup" />
                  Ketchup
                </label>
                <label>
                  <Field type="checkbox" name="sauces" value="mustard" />
                  Mustard
                </label>
                <label>
                  <Field type="checkbox" name="condiments" value="mayonnaise" />
                  Mayonnaise
                </label>
                <label>
                  <Field type="checkbox" name="condiments" value="guacamole" />
                  Guacamole
                </label>
              </div>
            </div>
            <div className="form-field">
              <label>Best Storage</label>
              <div className="best-storage-options">
                <label>
                  <Field type="radio" name="bestStorage" value="larry" />
                  larry
                </label>
                <label>
                  <Field type="radio" name="bestStorage" value="moe" />
                  Moe
                </label>
                <label>
                  <Field type="radio" name="bestStorage" value="curly" />
                  Curly
                </label>
              </div>
            </div>
            <div className="form-field">
              <label htmlFor="notes">Notes</label>
              <Field as="textarea" id="notes" name="notes" />
              <ErrorMessage name="notes" component="div" className="error" />
            </div>
            <div className="form-buttons">
              <button type="submit" disabled={isSubmitting || !isValid}>
                Submit
              </button>
              <button
                type="button"
                onClick={() => dirty && resetForm()}
                disabled={!dirty}
              >
                Reset
              </button>
            </div>
          </Form>
        )}
      </Formik>
      <div className="submitted-values">
        <h2>Submitted Values</h2>
        <pre style={{ textAlign: "start" }}>{submittedValues}</pre>
      </div>
    </div>
  );
};

export default UserForm;
