import React from "react";
import "./index.css";

interface Props {
  title: string;
}

function HeaderComponent(props: Props) {
  return <div className="header">{props.title || "Header title"}</div>;
}

export default HeaderComponent;
