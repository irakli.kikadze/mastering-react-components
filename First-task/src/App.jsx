import React from "react";
import "./App.css";
import FooterComponent from "./FooterComponent/FooterComponent.tsx";
import HeaderComponent from "./HeaderComponent/HeaderComponent.tsx";
import MainComponent from "./MainComponent/MainComponent.tsx";

function App() {
  return (
    <div className="App">
      <HeaderComponent title="Components Header"></HeaderComponent>
      <MainComponent></MainComponent>
      <FooterComponent></FooterComponent>
    </div>
  );
}

export default App;
